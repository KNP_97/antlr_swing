tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0; 
}

prog    : (e+=expr | d+=decl)*     -> template(name={$e},deklaracje={$d}) "<deklaracje>start: <name;separator=\" \n\"> ";

decl    : ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text})
        ; 
        catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)  -> arithmetic(p1={$e1.st}, p2={$e2.st}, op={"ADD"})
        | ^(MINUS e1=expr e2=expr)  -> arithmetic(p1={$e1.st}, p2={$e2.st}, op={"SUB"})
        | ^(MUL   e1=expr e2=expr)  -> arithmetic(p1={$e1.st}, p2={$e2.st}, op={"MUL"})
        | ^(DIV   e1=expr e2=expr)  -> arithmetic(p1={$e1.st}, p2={$e2.st}, op={"DIV"})
        | ^(PODST i1=ID   e2=expr)  -> store(name={$i1.text}, expr={$e2.st}, exists={locals.hasSymbol($i1.text)})
        | i2=ID                     -> load(name={$i2.text}, exists={locals.hasSymbol($i2.text)})
        | INT            {numer++;} -> int(i={$INT.text}, j={numer.toString()})
        | LB      {locals.enterScope();}
        | RB      {locals.leaveScope();}
        ;
        catch [RuntimeException ex] {errorID(ex,$i1);}